---
layout: post
title:  "Install Lanyon Theme for jekyll for Gitlab pages"
date:   2020-12-28 09:00:00 +0100
categories: jekyll
---

Update your system
```
sudo apt-get update
```

Perform Jekyll pre-requisites and installation
cf.
https://jekyllrb.com/docs/installation/ubuntu/

- Test your local install

```
tam@ux303:~/dev/lanyon$ bundle exec jekyll serve
Configuration file: /home/tam/dev/lanyon/_config.yml
            Source: /home/tam/dev/lanyon
       Destination: /home/tam/dev/lanyon/_site
 Incremental build: disabled. Enable with --incremental
      Generating...
       Jekyll Feed: Generating feed for posts
                    done in 0.336 seconds.
                    Auto-regeneration may not work on some Windows versions.
                    Please see: https://github.com/Microsoft/BashOnWindows/issues/216
                    If it does not work, please upgrade Bash on Windows or run Jekyll with --no-watch.
 Auto-regeneration: enabled for '/home/tam/dev/lanyon'
    Server address: http://127.0.0.1:4000/
  Server running... press ctrl-c to stop.
```
Once installation done it will update your Gemfile.
Then you can push it to your gitlab repository.

- rename public dir to assets for example and update path accordingly
cf. 
https://www.loumarven.dev/2020/02/23/getting-a-good-old-jekyll-theme-to-work-on-gitlab-pages/


