---
layout: post
title:  "Install Linux Subsystem for Windows 10"
date:   2020-12-28 16:40:00 +0100
categories: gitlab
---

In order to get a linux shell directly on my windows laptop, I decided to install Linux Subsystem:

Instructions can be found here:
- https://docs.microsoft.com/fr-fr/windows/wsl/install-win10#manual-installation-steps

However I as stuck with installation of `wsl_update_x64` which was saying : `This update only applies to machines with the Windows Subsytem for Linux` and applied the workaround described in https://github.com/microsoft/WSL/issues/5035#issuecomment-729914763
