---
layout: post
title:  "Install jekyll with Gitlab Pages"
date:   2020-12-27 16:40:00 +0100
categories: gitlab
---

Gitlab Pages can be installed by following https://gitlab.com/pages/jekyll

In a nutshell, you will need to:
- fork the repository
- **Remove the fork relationship** in `Settings` > `Advanced`
- edit a file, for example in the `_posts` directory to trigger a pipeline and get your pages published.
- your pages will be available at `<your gitlab userame>.gitlab.io/<your-project-name>`

Duration : 1 hour (reading documentation included)

note that it seems not possible to publish on github with a free account with a private repo.
But it could be possible with a public repo :
- https://medium.com/swlh/how-to-host-your-website-on-github-pages-for-free-3302b0fe8956
- https://www.toptal.com/github/unlimited-scale-web-hosting-github-pages-cloudflare
